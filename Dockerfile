FROM openjdk:8
MAINTAINER ecochap <ecochap@163.com>
VOLUME /tmp
ADD demo-0.0.1-SNAPSHOT.jar /data/teamblog.jar
EXPOSE 8888
RUN bash -c 'touch /data/teamblog.jar'
ENTRYPOINT ["java", "-jar", "/data/teamblog.jar"]