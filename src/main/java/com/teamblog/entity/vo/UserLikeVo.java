package com.teamblog.entity.vo;

import com.teamblog.entity.model.Blog;
import com.teamblog.entity.model.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

/**
 * @author MMMmoMan
 * @create 2021-07-19 23:08
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserLikeVo {

    private Integer id;
    private Blog blog;
    private User user;
    private Integer isDeleted;
    private Timestamp createTime;
    private Timestamp updateTime;

}
