package com.teamblog.entity.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author MMMmoMan
 * @create 2021-07-20 15:15
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BlogListVo {

    private Integer id;
    private String title;
    private String typeName;

}
