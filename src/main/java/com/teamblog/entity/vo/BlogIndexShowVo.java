package com.teamblog.entity.vo;

import com.teamblog.entity.model.Tag;
import com.teamblog.entity.model.Type;
import com.teamblog.entity.model.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.List;

/**
 * @author MMMmoMan
 * @create 2021-07-20 22:56
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BlogIndexShowVo {

    private Integer id;
    private String title;
    private String content;
    private Integer views;
    private Timestamp updateTime;
    private User user;
    private Type type;
    private List<Tag> tags;

}
