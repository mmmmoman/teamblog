package com.teamblog.entity.vo;

import com.teamblog.entity.model.Blog;
import com.teamblog.entity.model.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

/**
 * @author MMMmoMan
 * @create 2021-07-19 23:07
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserHistoryVo {

    private Integer id;
    private User user;
    private Blog blog;
    private Timestamp createTime;
    private Timestamp updateTime;

}
