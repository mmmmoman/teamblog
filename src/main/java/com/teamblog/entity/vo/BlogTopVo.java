package com.teamblog.entity.vo;

import com.teamblog.entity.model.Blog;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

/**
 * @author MMMmoMan
 * @create 2021-07-19 23:08
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BlogTopVo {

    private Integer id;
    private Blog blog;
    private Integer isTop;
    private Timestamp createTime;
    private Timestamp updateTime;

}
