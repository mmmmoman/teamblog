package com.teamblog.entity.vo;

import com.teamblog.entity.model.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Time;
import java.sql.Timestamp;

/**
 * @author MMMmoMan
 * @create 2021-07-19 23:07
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserSubscribeVo {

    private Integer id;
    private User subscribed;
    private User subscriber;
    private Integer isDeleted;
    private Timestamp createTime;
    private Timestamp updateTime;


}
