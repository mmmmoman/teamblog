package com.teamblog.entity.param;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author MMMmoMan
 * @create 2021-07-20 22:48
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TypeTop {
    private Integer id;
    private String name;
    private Integer blogsnum;

}
