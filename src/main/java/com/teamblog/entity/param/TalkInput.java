package com.teamblog.entity.param;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author koitomi
 * @since 2021/7/20 18:44
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TalkInput {

    private String content;
    private Integer userId;

}
