package com.teamblog.entity.param;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author MMMmoMan
 * @create 2021-07-20 14:04
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BlogInput {

    private Integer id;
    private String title;
    private String content;
    private Integer typeId;
    private List<Integer> tagIds;


}
