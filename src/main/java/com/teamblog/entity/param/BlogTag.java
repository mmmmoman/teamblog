package com.teamblog.entity.param;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author MMMmoMan
 * @create 2021-07-20 10:07
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BlogTag {

    private Long id;
    private Long blogId;
    private Long TagId;

}
