package com.teamblog.entity.param;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author lixin
 * @since 2021/7/21 21:46
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LikeBlog {

    private Integer blogId;
    private Integer userId;

}
