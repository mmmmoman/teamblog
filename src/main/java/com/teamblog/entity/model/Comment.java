package com.teamblog.entity.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

/**
 * @author MMMmoMan
 * @create 2021-07-19 23:01
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Comment {

    private Integer id;
    private String content;
    private Integer userId;
    private Integer blogId;
    private Timestamp createTime;
    private Timestamp updateTime;

}
