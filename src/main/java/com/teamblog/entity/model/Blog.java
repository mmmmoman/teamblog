package com.teamblog.entity.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * @author MMMmoMan
 * @create 2021-07-19 22:55
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Blog {

    private Integer id;
    private String title;
    private String content;
    private Integer views;
    private Integer typeId;
    private Integer userId;
    private Integer isDeleted;
    private Timestamp createTime;
    private Timestamp updateTime;

    private String tagIds;
    private List<Tag> tags = new ArrayList<>();


    //调用下面那个方法，初始化tagIds属性
    public void init(){
        this.tagIds = tagsToIds(this.getTags());
    }

    //用于将数据库字段tags转换为字符串的形式，返还给前端页面用的
    public String tagsToIds(List<Tag> tags){
        if(!tags.isEmpty()){
            StringBuffer ids = new StringBuffer();
            boolean flag = false;
            for (Tag tag : tags) {
                if(flag){
                    ids.append(",");
                }else{
                    flag = true;
                }
                ids.append(tag.getId());
            }
            return ids.toString();
        }
        else return tagIds;
    }


}
