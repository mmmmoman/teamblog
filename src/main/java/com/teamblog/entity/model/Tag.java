package com.teamblog.entity.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author MMMmoMan
 * @create 2021-07-19 22:56
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Tag {

    private Integer id;
    private String name;
    private Integer isDeleted;

}
