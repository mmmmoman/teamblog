package com.teamblog.entity.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

/**
 * @author lixin
 * @since 2021/7/21 11:17
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserLike {

    private Integer id;
    private Integer blogId;
    private Integer userId;
    private Integer isDeleted;
    private Timestamp createTime;
    private Timestamp updateTime;

}
