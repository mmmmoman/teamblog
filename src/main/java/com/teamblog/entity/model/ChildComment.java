package com.teamblog.entity.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

/**
 * @author MMMmoMan
 * @create 2021-07-19 23:03
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ChildComment {

    private Integer id;
    private Integer parentId;
    private String content;
    private Integer userId;
    private Timestamp createTime;
    private Timestamp updateTime;

}
