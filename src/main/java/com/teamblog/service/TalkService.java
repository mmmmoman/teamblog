package com.teamblog.service;

import com.github.pagehelper.PageInfo;
import com.teamblog.entity.param.TalkInput;
import com.teamblog.entity.vo.UserTalkVo;

import java.util.List;

/**
 * @author koitomi
 * @since 2021/7/20 15:53
 */
public interface TalkService {
    PageInfo<UserTalkVo> listAll(Integer pageNum, Integer pageSize);
    boolean saveTalk(TalkInput talkInput);
    List<UserTalkVo> listPersonalTalk(Integer id);
    boolean deleteTalk(Integer id);
}
