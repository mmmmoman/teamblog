package com.teamblog.service;

import com.teamblog.entity.model.User;

/**
 * @author MMMmoMan
 * @create 2021-07-19 23:32
 */
public interface UserService {

    /**
     * 验证用户名密码是否正确
     * @param username
     * @param password
     * @return
     */
    Boolean checkUser(String username, String password);

    /**
     * 保存新用户信息
     * @param user
     * @return
     */
    Boolean saveUser(User user);

    /**
     * 验证用户名是否存在
     * @param username
     * @return
     */
    Boolean checkUsernameIfExist(String username);

    /**
     * 根据id获取用户信息
     * @param id
     * @return
     */
    User getUserById(Integer id);

    /**
     * 根据用户名和密码获取用户信息
     * @param username
     * @param password
     * @return
     */
    User getUserByUsernameAndPassword(String username, String password);

    /**
     * 更新用户信息
     * @param user
     * @return
     */
    Boolean updateUser(User user);

    /**
     * 根据id删除用户信息
     * @param id
     * @return
     */
    Boolean removeUserById(Integer id);

}
