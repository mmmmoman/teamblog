package com.teamblog.service;


import com.teamblog.entity.model.Blog;
import com.teamblog.entity.param.BlogTag;

import java.util.List;

/**
 * @create 2021-06-28 20:41
 */
public interface BlogTagService {

    void saveBlogTag(Integer blogId,Integer tagId);

    BlogTag findAll();

    List<Integer> findBlogIdsByTag(Integer tagId);

    void initBlogTags(List<Blog> blogs);

    void updateBlogTag(Integer blogId,Integer tagId);

    void deleteByBlog(Integer blogId);

}
