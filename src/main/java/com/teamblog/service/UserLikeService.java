package com.teamblog.service;

/**
 * @author lixin
 * @since 2021/7/21 17:17
 */
public interface UserLikeService {

    /**
     * 根据博客id查询所有点赞记录
     * 并存入原始点赞集合缓存
     * @param blogId
     */
    void listAllLikeByBlogIdTocCache(Integer blogId);

    /**
     * 点赞方法
     * @param userId
     * @param blogId
     */
    void like(Integer userId, Integer blogId);

    /**
     * 根据博客id查询缓存中点赞量
     * @param blogId
     * @return
     */
    Long countCacheLikeByBlogId(Integer blogId);

    /**
     * 更新点赞记录
     * @param blogId
     * @param userId
     */
    void updateLike(Integer blogId, Integer userId);

}
