package com.teamblog.service;


import com.teamblog.entity.model.Tag;

import java.util.List;

/**
 * @create 2021-06-21 15:57
 */
public interface TagService {

    void saveTag(Tag tag);

    Tag getTag(Integer id);

    List<Tag> listTag();

    List<Tag> listTags(String ids);

    List<Tag> listTagsTop(Integer num);

    void updateTag(Integer id,Tag tag);

    void deleteTag(Integer id);

    Tag findTagByName(String name);

}
