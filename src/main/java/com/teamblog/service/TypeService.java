package com.teamblog.service;


import com.teamblog.entity.model.Type;
import com.teamblog.entity.param.TypeTop;

import java.util.List;

/**
 * @create 2021-06-21 13:09
 */
public interface TypeService {

    void saveType(Type type);

    Type getType(Integer id);

    List<Type> listType();

    List<TypeTop> listTypeTop(Integer num);

    void updateType(Integer id,Type type);

    void deleteType(Integer id);

    Type findTypeByName(String name);

}
