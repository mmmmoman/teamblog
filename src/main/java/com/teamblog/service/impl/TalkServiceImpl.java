package com.teamblog.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.teamblog.dao.TalkMapper;
import com.teamblog.entity.param.TalkInput;
import com.teamblog.entity.vo.UserTalkVo;
import com.teamblog.service.TalkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author koitomi
 * @since 2021/7/20 15:53
 */
@Service
public class TalkServiceImpl implements TalkService {

    @Autowired
    TalkMapper talkMapper;

    @Transactional
    @Override
    public PageInfo<UserTalkVo> listAll(Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<UserTalkVo> list = talkMapper.selectAll();
        return new PageInfo<>(list);
    }

    @Override
    public boolean saveTalk(TalkInput talkInput) {
        return talkMapper.InsertNewTalk(talkInput) > 0 ? true :false;
    }

    @Override
    public List<UserTalkVo> listPersonalTalk(Integer id) {

        return talkMapper.getTalkListByUserId(id);
    }

    @Override
    public boolean deleteTalk(Integer id) {

        return talkMapper.deleteTalk(id) > 0 ? true:false;
    }
}
