package com.teamblog.service.impl;

import com.teamblog.dao.UserMapper;
import com.teamblog.entity.model.User;
import com.teamblog.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author MMMmoMan
 * @create 2021-07-19 23:33
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserMapper userMapper;

    @Override
    public Boolean checkUser(String username, String password) {
        return userMapper.selectUserByUsernameAndPassword(username, password) != null;
    }

    @Override
    public Boolean saveUser(User user) {
        return userMapper.insertUser(user) > 0;
    }

    @Override
    public Boolean checkUsernameIfExist(String username) {
        return userMapper.countByUsername(username) > 0;
    }

    @Override
    public User getUserById(Integer id) {
        return userMapper.selectUserById(id);
    }

    @Override
    public User getUserByUsernameAndPassword(String username, String password) {
        return userMapper.selectUserByUsernameAndPassword(username, password);
    }

    @Override
    public Boolean updateUser(User user) {
        return userMapper.updateUser(user) > 0;
    }

    @Override
    public Boolean removeUserById(Integer id) {
        return userMapper.deleteUserById(id) > 0;
    }
}
