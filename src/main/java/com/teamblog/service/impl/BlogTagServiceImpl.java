package com.teamblog.service.impl;

import com.teamblog.dao.BlogMapper;
import com.teamblog.dao.BlogTagMapper;
import com.teamblog.dao.TagMapper;
import com.teamblog.entity.model.Blog;
import com.teamblog.entity.model.Tag;
import com.teamblog.entity.param.BlogTag;
import com.teamblog.service.BlogTagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @create 2021-06-28 20:41
 */
@Service
public class BlogTagServiceImpl implements BlogTagService {

    @Autowired
    private BlogTagMapper blogTagMapper;

    @Autowired
    private BlogMapper blogMapper;

    @Autowired
    private TagMapper tagMapper;

    @Override
    public void saveBlogTag(Integer blogId, Integer tagId) {
        blogTagMapper.saveBlogTag(blogId,tagId);
    }

    @Override
    public BlogTag findAll() {
        return blogTagMapper.findAll();
    }

    @Override
    public List<Integer> findBlogIdsByTag(Integer tagId) {
        return blogTagMapper.findBlogIdsByTag(tagId);
    }

    @Override
    public void initBlogTags(List<Blog> blogs) {//初始化每个blog内的tags
        for (Blog blog : blogs) {
            List<Tag> tags = new ArrayList<>();
            List<Integer> tagIds = blogTagMapper.findTagIdsByBlog(blog.getId());
            for (Integer tagId : tagIds) {
                Tag tag = tagMapper.getTag(tagId);
                tags.add(tag);
            }
            blog.setTags(tags);
        }

    }

    @Override
    public void updateBlogTag(Integer blogId, Integer tagId) {
        blogTagMapper.updateBlogTag(blogId,tagId);
    }

    @Override
    public void deleteByBlog(Integer blogId) {
        blogTagMapper.deleteByBlog(blogId);
    }
}
