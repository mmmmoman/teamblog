package com.teamblog.service.impl;

import com.teamblog.dao.UserLikeMapper;
import com.teamblog.entity.model.UserLike;
import com.teamblog.service.UserLikeService;
import com.teamblog.utils.RedisKeyUtils;
import com.teamblog.utils.RedisUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author lixin
 * @since 2021/7/21 20:33
 */
@Service
public class UserLikeServiceImpl implements UserLikeService {

    @Autowired
    private RedisUtils redisUtils;

    @Autowired
    private UserLikeMapper userLikeMapper;

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    @Override
    public void listAllLikeByBlogIdTocCache(Integer blogId) {
        List<UserLike> likeList = userLikeMapper.selectAllByBlogId(blogId);
        for (UserLike like : likeList) {
            String blogKey = RedisKeyUtils.getPrefixLikeKey(blogId);
            redisTemplate.opsForSet().add(blogKey, like.getUserId());
        }
    }

    @Override
    public void like(Integer userId, Integer blogId) {
        // 获得点赞博客的key
        String blogKey = RedisKeyUtils.getPrefixLikeKey(blogId);
        // 判断指定用户是否点赞过指定博客
        boolean isMember = redisTemplate.opsForSet().isMember(blogKey, userId);
        // 已点过赞
        if (isMember) {
            redisTemplate.opsForSet().remove(blogKey, userId);
        } else {
            redisTemplate.opsForSet().add(blogKey, userId);
        }
    }

    @Override
    public Long countCacheLikeByBlogId(Integer blogId) {
        String blogKey = RedisKeyUtils.getPrefixLikeKey(blogId);
        return redisTemplate.opsForSet().size(blogKey);
    }

    @Override
    public void updateLike(Integer blogId, Integer userId) {
        UserLike userLike = userLikeMapper.selectOneByBlogIdUserId(blogId, userId);
        if (userLike == null) {
            UserLike n = new UserLike();
            n.setBlogId(blogId);
            n.setUserId(userId);
            userLikeMapper.insertUserLike(n);
        } else {
            userLike.setIsDeleted(0);
            userLikeMapper.updateUserLike(userLike);
        }
    }
}
