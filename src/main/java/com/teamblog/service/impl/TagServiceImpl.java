package com.teamblog.service.impl;

import com.teamblog.common.exception.NotFoundException;
import com.teamblog.dao.TagMapper;
import com.teamblog.entity.model.Tag;
import com.teamblog.service.TagService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @create 2021-06-21 15:58
 */
@Service
public class TagServiceImpl implements TagService {
    @Autowired
    private TagMapper tagMapper;

    @Override
    public void saveTag(Tag tag) {
        tagMapper.saveTag(tag);
    }

    @Override
    public Tag getTag(Integer id) {
        return tagMapper.getTag(id);
    }

    @Override
    public List<Tag> listTag() {
        return tagMapper.listTag();
    }

    @Override
    public List<Tag> listTags(String ids) {
        return tagMapper.listTags(convertToList(ids));
    }

    @Override
    public List<Tag> listTagsTop(Integer num) {
        return tagMapper.listTagsTop(num);
    }

    private  List<Integer> convertToList(String ids){//把形如“1，2，3”的String转换为数组
        List<Integer> list = new ArrayList<>();
        if(!"".equals(ids) && ids!= null){
            String[] idarray = ids.split(",");
            for (int i = 0; i < idarray.length; i++) {
                list.add(new Integer(idarray[i]));
            }
        }
        return list;
    }

    @Override
    public void updateTag(Integer id, Tag type) {
        Tag t = tagMapper.getTag(id);
        if(t == null){
            throw new NotFoundException("不存在的类型");
        }
        BeanUtils.copyProperties(type,t);

        tagMapper.updateTag(id,t);
    }

    @Override
    public void deleteTag(Integer id) {
        tagMapper.deleteTag(id);
    }

    @Override
    public Tag findTagByName(String name) {
        return tagMapper.findTagByName(name);
    }
}
