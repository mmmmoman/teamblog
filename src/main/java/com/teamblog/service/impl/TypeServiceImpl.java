package com.teamblog.service.impl;

import com.teamblog.common.exception.NotFoundException;
import com.teamblog.dao.TypeMapper;
import com.teamblog.entity.model.Type;
import com.teamblog.entity.param.TypeTop;
import com.teamblog.service.TypeService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @create 2021-06-21 13:14
 */
@Service
public class TypeServiceImpl  implements TypeService {

    @Autowired
    private TypeMapper typeMapper;

    @Override
    public void saveType(Type type) {
        typeMapper.saveType(type);
    }

    @Override
    public Type getType(Integer id) {
        return typeMapper.getType(id);
    }

    @Override
    public List<Type> listType() {
        return typeMapper.listType();
    }

    @Transactional
    @Override
    public List<TypeTop> listTypeTop(Integer num) {
        return typeMapper.listTypeTop(num);
    }

    @Override
    public void updateType(Integer id, Type type) {
        Type t = typeMapper.getType(id);
        if(t == null){
            throw new NotFoundException("不存在的类型");
        }
        BeanUtils.copyProperties(type,t);

        typeMapper.updateType(id,t);
    }

    @Override
    public void deleteType(Integer id) {
        typeMapper.deleteType(id);
    }

    @Override
    public Type findTypeByName(String name) {
        return typeMapper.findTypeByName(name);
    }
}
