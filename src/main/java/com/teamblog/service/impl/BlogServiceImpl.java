package com.teamblog.service.impl;

import com.teamblog.common.exception.NotFoundException;
import com.teamblog.dao.BlogMapper;
import com.teamblog.dao.BlogTagMapper;
import com.teamblog.entity.model.Blog;
import com.teamblog.entity.model.Tag;
import com.teamblog.entity.param.BlogQuery;
import com.teamblog.service.BlogService;
import com.teamblog.service.UserLikeService;
import com.teamblog.utils.MarkdownUtils;
import com.teamblog.utils.RedisUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @create 2021-06-21 19:27
 */
@Service
public class BlogServiceImpl implements BlogService {

    @Autowired
    private BlogMapper blogMapper;

    @Autowired
    private BlogTagMapper blogTagMapper;

    @Autowired
    private RedisUtils redisUtils;

    @Autowired
    private UserLikeService userLikeService;

    @Transactional
    @Override
    public Blog getBlog(Integer id) {
        /*-------------判断缓存中是否有当前博客----------------*/
        Blog blog = (Blog) redisUtils.get("blog:" + id);
        if (blog == null) { // 缓存中没有当前博客记录
            /*-----------从数据库中读取数据并存到缓存中-------------*/
            Blog dbBlog = blogMapper.getBlog(id);
            if (dbBlog != null) {
                // 将查询到的博客放入缓存
                redisUtils.set("blog:" + dbBlog.getId(), dbBlog);
                blog = dbBlog;
            } else {
                throw new NotFoundException();
            }
        }
        return blog;
    }

    @Transactional
    @Override
    public Blog getAndConvert(Integer id) {//获取blog后将其content转义
        Blog blog = getBlog(id);
        Blog b = new Blog();
        BeanUtils.copyProperties(blog,b);
        String content = b.getContent();
        b.setContent(MarkdownUtils.markdownToHtmlExtensions(content));
        return b;

    }

    @Override
    public List<Blog> getBlogByUser(Integer id) {
        return blogMapper.getBlogByUser(id);
    }

    @Override
    public void updateBlogViews(Integer blogId, Integer views) {
        Blog blog = blogMapper.getBlog(blogId);
        if (blog != null) {
            blogMapper.updateViews(blogId, views);
        }
    }

    @Transactional
    @Override
    public List<Blog> listBlog(BlogQuery blog) {
        return blogMapper.listBlog(blog);
    }

    @Transactional
    @Override
    public List<Blog> listBlogTop(Integer num) {
        return blogMapper.listBlogTop(num);
    }

    @Transactional
    @Override
    public List<Blog> listBlogQuery(String query) {
        return blogMapper.listBlogQuery(query);
    }

    @Transactional
    @Override
    public List<Blog> listBlogByType(Integer typeId) {
        return blogMapper.listBlogByType(typeId);
    }

    @Transactional
    @Override
    public List<Blog> listBlogByTag(Integer tagId) {
        return null;
    }


    @Transactional
    @Override
    public Integer saveBlog(Blog blog) {
        if(blog.getId() == null){//新增blog需要进行初始化
            blog.setCreateTime(new Timestamp(new Date().getTime()));
            blog.setUpdateTime(new Timestamp(new Date().getTime()));
            blog.setViews(1);
            blog.setIsDeleted(1);
        }
        //将blog保存到blog表
        blogMapper.saveBlog(blog);
        return blog.getId();
    }

    @Transactional
    @Override
    public void updateBlog(Integer id, Blog blog) {
        Blog b = blogMapper.getBlog(id);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        if(b == null){
            throw new NotFoundException("没有找到博客");
        }
        BeanUtils.copyProperties(blog,b);
        b.setUpdateTime(new Timestamp(new Date().getTime()));
        blogMapper.updateBlog(id,b);

        List<Tag> tags = blog.getTags();//修改blogTag表
        for (Tag tag : tags) {
            blogTagMapper.updateBlogTag(blog.getId(),tag.getId());
        }
        /*-------------删除缓存-------------*/
        redisUtils.del("blog:" + b.getId());
    }

    /**
     * 删除博客涉及博客表和博客标签表
     * 多表修改使用事务保证操作的原子性
     * @param id
     */
    @Transactional
    @Override
    public void deleteBlog(Integer id) {
        // 删除blog
        blogMapper.deleteBlog(id);
        // 删除blog对应的blog_tag
        blogTagMapper.deleteByBlog(id);
        /*-------------删除缓存-------------*/
        redisUtils.del("blog:" + id);
    }

    @Transactional
    @Override
    public List<Blog> findAllBlog() {
        return blogMapper.findAllBlog();
    }

    @Override
    public void increaseCacheBlogViewsById(Integer blogId) {
        /*--------------------增加访问量-------------------*/
        // 获取缓存中的访问量
        Integer views = (Integer) redisUtils.get("blog:" + blogId + ":views:");
        if (views == null) { // 缓存中没有当前博客的访问量记录
            /*-------------判断缓存中是否有当前博客----------------*/
            Blog blog = (Blog) redisUtils.get("blog:" + blogId);
            if (blog != null) {
                // 使用缓存来存储博客访问量
                redisUtils.set("blog:" + blogId + ":views:", blog.getViews());
            } else {
                // 从数据库中查询博客
                blog = blogMapper.getBlog(blogId);
                if (blog != null) {
                    redisUtils.set("blog:" + blogId + ":views:", blog.getViews());
                }
            }
        }
        // 缓存中访问量自增1
        redisUtils.incr("blog:" + blogId + ":views:", 1);
    }
}
