package com.teamblog.service;


import com.teamblog.entity.model.Blog;
import com.teamblog.entity.param.BlogQuery;

import java.util.List;

/**
 * @create 2021-06-21 19:26
 */
public interface BlogService {

    Blog getBlog(Integer id);

    Blog getAndConvert(Integer id);

    List<Blog> getBlogByUser(Integer id);

    List<Blog> listBlog(BlogQuery blog);

    List<Blog> listBlogTop(Integer num);

    List<Blog> listBlogQuery(String query);

    List<Blog> listBlogByType(Integer typeId);

    List<Blog> listBlogByTag(Integer tagId);

    Integer saveBlog(Blog blog);

    void updateBlog(Integer id,Blog blog);

    void deleteBlog(Integer id);

    List<Blog> findAllBlog();

    /**
     * 更新博客访问量为新值
     * @param blogId
     * @param views
     */
    void updateBlogViews(Integer blogId, Integer views);

    /**
     * 将缓存中的博客访问量增加1
     * 若缓存没有数据则从数据库查询，再放入缓存
     * @param blogId
     */
    void increaseCacheBlogViewsById(Integer blogId);

}
