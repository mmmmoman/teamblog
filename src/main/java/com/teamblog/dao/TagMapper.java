package com.teamblog.dao;

import com.teamblog.entity.model.Tag;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @create 2021-06-21 15:59
 */
@Mapper
@Repository
public interface TagMapper {
    void saveTag(Tag tag);

    Tag getTag(Integer id);

    List<Tag> listTag();

    List<Tag> listTags(@Param("tagList") List<Integer> tagList);

    List<Tag> listTagsTop(Integer num);

    void updateTag(Integer id,Tag tag);

    void deleteTag(Integer id);

    Tag findTagByName(String name);
}
