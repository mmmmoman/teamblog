package com.teamblog.dao;

import com.teamblog.entity.param.BlogTag;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @create 2021-06-28 19:30
 */
@Mapper
@Repository
public interface BlogTagMapper {

    void saveBlogTag(@Param("blogId") Integer blogId,@Param("tagId") Integer tagId);

    BlogTag findAll();

    void updateBlogTag(@Param("blogId") Integer blogId,@Param("tagId") Integer tagId);

    void deleteByBlog(@Param("blogId") Integer blogId);

    List<Integer> findBlogIdsByTag(@Param("tagId")Integer tagId);

    List<Integer> findTagIdsByBlog(@Param("blogId") Integer blogId);

}
