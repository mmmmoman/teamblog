package com.teamblog.dao;

import com.teamblog.entity.model.User;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @author MMMmoMan
 * @create 2021-07-19 23:34
 */
@Mapper
@Repository
public interface UserMapper {

    /**
     * 新增用户
     * @param user
     * @return
     */
    Integer insertUser(User user);

    /**
     * 根据id删除用户
     * @param id
     * @return
     */
    Integer deleteUserById(Integer id);

    /**
     * 更新用户信息
     * @param user
     * @return
     */
    Integer updateUser(User user);

    /**
     * 统计指定用户名的用户个数，用来校验用户名是否重复
     * @param username
     * @return
     */
    Integer countByUsername(String username);

    /**
     * 根据id查询用户信息
     * @param id
     * @return
     */
    User selectUserById(Integer id);

    /**
     * 根据用户名和密码查询用户信息
     * @param username
     * @param password
     * @return
     */
    User selectUserByUsernameAndPassword(String username, String password);

}
