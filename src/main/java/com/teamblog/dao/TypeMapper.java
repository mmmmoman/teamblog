package com.teamblog.dao;

import com.teamblog.entity.model.Type;
import com.teamblog.entity.param.TypeTop;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @create 2021-06-20 17:15
 */
@Mapper
@Repository
public interface TypeMapper {

    void saveType(Type type);

    Type getType(Integer id);

    List<Type> listType();

    List<TypeTop> listTypeTop(Integer num);

    void updateType(@Param("id") Integer id,@Param("type") Type type);

    void deleteType(Integer id);

    Type findTypeByName(String name);

}
