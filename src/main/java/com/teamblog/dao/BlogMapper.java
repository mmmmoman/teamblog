package com.teamblog.dao;

import com.teamblog.entity.model.Blog;
import com.teamblog.entity.param.BlogQuery;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @create 2021-06-21 19:28
 */
@Mapper
@Repository
public interface BlogMapper {
    Blog getBlog(Integer id);

    /**
     * 更新博客访问量
     * 直接设置为新值
     * @param id
     * @param views
     */
    void updateViews(@Param("id") Integer id, @Param("views") Integer views);

    List<Blog> findAllBlog();

    List<Blog> getBlogByUser(Integer id);

    List<Blog> listBlog(BlogQuery blog);

    List<Blog> listBlogTop(Integer num);

    List<Blog> listBlogQuery(String query);

    List<Blog> listBlogByType(Integer typeId);

    List<Blog> listBlogByTag(Integer tagId);

    Integer saveBlog(Blog blog);

    void updateBlog(@Param("id") Integer id,@Param("blog") Blog blog);

    void deleteBlog(Integer id);

}
