package com.teamblog.dao;

import com.teamblog.entity.param.TalkInput;
import com.teamblog.entity.vo.UserTalkVo;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author koitomi
 * @since 2021/7/20 16:04
 */

@Mapper
@Repository
public interface TalkMapper {

    List<UserTalkVo> selectAll();

    int InsertNewTalk(TalkInput talkInput);

    List<UserTalkVo> getTalkListByUserId(Integer id);

    int deleteTalk(Integer id);

}
