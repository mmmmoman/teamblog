package com.teamblog.dao;

import com.teamblog.entity.model.UserLike;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author lixin
 * @since 2021/7/21 11:19
 */
@Mapper
@Repository
public interface UserLikeMapper {

    /**
     * 新增点赞记录
     * @param userLike
     * @return
     */
    Integer insertUserLike(UserLike userLike);

    /**
     * 根据id删除点赞记录
     * 逻辑删除
     * @param id
     * @return
     */
    Integer deleteOneById(Integer id);

    /**
     * 根据博客id删除对应全部点赞记录
     * 逻辑删除
     * @param blogId
     * @return
     */
    Integer deleteAllByBlogId(Integer blogId);

    /**
     * 根据用户id删除对应全部点赞记录
     * 逻辑删除
     * @param userId
     * @return
     */
    Integer deleteAllByUserId(Integer userId);

    /**
     * 根据id查询
     * @return
     */
    UserLike selectOneById();

    /**
     * 根据博客id和用户id查询点赞记录
     * @param blogId
     * @param userId
     * @return
     */
    UserLike selectOneByBlogIdUserId(Integer blogId, Integer userId);

    /**
     * 根据博客id查询全部点赞记录
     * 忽略is_deleted=1已逻辑删除的记录
     * @param blogId
     * @return
     */
    List<UserLike> selectAllByBlogId(Integer blogId);

    /**
     * 根据用户id询全部点赞记录
     * @param userId
     * @return
     */
    List<UserLike> selectAllByUserId(Integer userId);

    /**
     * 更新点赞记录
     * @param userLike
     * @return
     */
    Integer updateUserLike(UserLike userLike);

    /**
     * 根据博客id统计点赞量
     * 忽略is_deleted=0的记录
     * @param blogId
     * @return
     */
    Long countByBlogId(Integer blogId);

}
