package com.teamblog.common.enums;

/**
 * @author lixin
 * @since 2021/6/7 10:16
 */
public enum CodeEnum {

    SUCCESS(1000, "成功"),
    FAIL(1001, "失败"),
    DATABASE_ERROR(1002,"数据库执行出错"),
    REDIS_ERROR(1003,"Redis缓存执行出错"),
    PARAMETER_VALUE_INVALID(1004,"参数的值不合法"),
    PARAMETER_MISSING(1005,"请求参数不全"),
    PARAMETER_ILLEGAL(1006,"请求参数不合法"),
    LOGIN_FAIL_HAVE_LOGIN(1007,"被登录了，强制退出登录"),
    WITHOUT_AUTHORITY(2000,"没有权限"),
    NO_HAVE_TOKEN(2001,"缺少token"),

    UNSUPPORTED_FILE_TYPE(3001,"不支持的文件类型"),
    IO_ERROR(3002,"后台IO出错"),

    FREQUENT_REQUESTS(4000,"过于频繁的请求"),

    OTHER_ERROR(5000,"其他(未知)错误"),

    USER_IS_EXISTED(6000, "用户名已存在");

    // 错误码
    private final Integer code;
    // 错误信息
    private final String message;

    CodeEnum(Integer code, String message){
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
