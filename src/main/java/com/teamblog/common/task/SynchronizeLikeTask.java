package com.teamblog.common.task;

import com.teamblog.service.BlogService;
import com.teamblog.service.UserLikeService;
import com.teamblog.utils.RedisUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Set;

/**
 * @author lixin
 * @since 2021/7/21 22:02
 */
@Component
public class SynchronizeLikeTask {

    @Autowired
    private RedisUtils redisUtils;

    @Autowired
    UserLikeService userLikeService;

    @Autowired
    BlogService blogService;

    // cron表达式参数
    // 秒(Seconds,0~59的整数)、
    // 分(Minutes,0~59的整数)、
    // 小时(Hours,0~23的整数)、
    // 日期(DayofMonth,1~31的整数,与该月天数有关)、
    // 月份(Month,1~12的整数或JAN-DEC)、
    // 星期(DayofWeek,1~7的整数或者 SUN-SAT[1=SUN])、
    // 年(Year,可选,1970~2099)

    /**
     * 定时同步点赞记录到数据库
     */
    @Scheduled(cron = "0 0 0 */1 * ?") // 每天0点执行
    public void synchronizeLikeTask(){
        String pattern = "like:blog:*";
        Set<String> keys = redisUtils.getKeys(pattern);
        for (String key : keys) {
            String[] split = key.split(":");
            int blogId = Integer.parseInt(split[2]);
            Set<Object> userIdSet = redisUtils.sGet("like:blog:" + blogId);
            for (Object o : userIdSet) {
                Integer userId = (Integer) o;
                userLikeService.updateLike(blogId, userId);
            }
        }
    }

    /**
     * 定时同步访问量到数据库
     */
    @Scheduled(cron = "0 0 */1 * * ?") // 每小时0分0秒执行
    public void synchronizeViewTask(){
        String pattern = "blog:*:views";
        // 获取缓存中博客访问量的所有键
        Set<String> keys = redisUtils.getKeys(pattern);
        for (String key : keys) {
            String[] split = key.split(":");
            // 获取博客id
            int blogId = Integer.parseInt(split[1]);
            // 获取缓存中博客访问量的值
            int views = (int) redisUtils.get("blog:" + blogId + ":views");
            // 更新访问量到数据库
            blogService.updateBlogViews(blogId, views);
            // 删除缓存
            redisUtils.del(key);
        }
    }

}
