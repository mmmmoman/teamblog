package com.teamblog.utils;

/**
 * @author lixin
 * @since 2021/7/21 23:07
 */
public class RedisKeyUtils {

    private static final String SPLIT = ":";

    private static final String PREFIX_LIKE = "like:blog";

    // 得到key
    public static String getPrefixLikeKey(Integer blogId){
        return PREFIX_LIKE + SPLIT + blogId;
    }

}
