package com.teamblog.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;


import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * @author lixin
 * @since 2021/6/25 19:54
 */
@Slf4j
@Component
public class RedisUtils {

    private JedisPool jedisPool;

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;


    public RedisUtils(@Value("${spring.redis.host}") String host, @Value("${spring.redis.port}") Integer port,
                      @Value("${spring.redis.password}") String password, @Value("${spring.redis.timeout}") Integer timeout) {
        JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
        jedisPoolConfig.setMaxWaitMillis(timeout);
        jedisPool = new JedisPool(jedisPoolConfig, host, port, timeout, password);
    }

    public Jedis getResource() {
        return jedisPool.getResource();
    }

    //=============================common============================

    /**
     * 指定缓存失效时间
     *
     * @param key  键
     * @param time 时间(秒)
     * @return
     */
    public boolean expire(String key, long time, TimeUnit timeUnit) {
        try {
            if (time > 0) {
                redisTemplate.expire(key, time, timeUnit);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 根据key 获取过期时间
     *
     * @param key 键 不能为null
     * @return 时间(秒) 返回0代表为永久有效
     */
    public long getExpire(String key) {
        return redisTemplate.getExpire(key, TimeUnit.SECONDS);
    }

    /**
     * 判断key是否存在
     *
     * @param key 键
     * @return true 存在 false不存在
     */
    public boolean hasKey(String key) {
        try {
            return redisTemplate.hasKey(key);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 删除缓存
     *
     * @param key 可以传一个值 或多个
     */
    @SuppressWarnings("unchecked")
    public void del(String... key) {
        if (key != null && key.length > 0) {
            if (key.length == 1) {
                redisTemplate.delete(key[0]);
            } else {
                redisTemplate.delete((Collection<String>) CollectionUtils.arrayToList(key));///////////
            }
        }
    }

    //============================String=============================

    /**
     * 普通缓存获取
     *
     * @param key 键
     * @return 值
     */
    public Object get(String key) {
        return key == null ? null : redisTemplate.opsForValue().get(key);
    }

    /**
     * 普通缓存放入
     *
     * @param key   键
     * @param value 值
     * @return true成功 false失败
     */
    public boolean set(String key, Object value) {
        try {
            redisTemplate.opsForValue().set(key, value);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    /**
     * 普通缓存放入并设置时间
     *
     * @param key   键
     * @param value 值
     * @param time  时间(秒) time要大于0 如果time小于等于0 将设置无限期
     * @return true成功 false 失败
     */
    public boolean set(String key, Object value, long time, TimeUnit timeUnit) {
        try {
            if (time > 0) {
                redisTemplate.opsForValue().set(key, value, time, timeUnit);
            } else {
                set(key, value);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 递增
     *
     * @param key 键
     * @return
     */
    public long incr(String key, long delta) {
        if (delta < 0) {
            throw new RuntimeException("递增因子必须大于0");
        }
        return redisTemplate.opsForValue().increment(key, delta);
    }

    /**
     * 递减
     *
     * @param key 键
     * @return
     */
    public long decr(String key, long delta) {
        if (delta < 0) {
            throw new RuntimeException("递减因子必须大于0");
        }
        return redisTemplate.opsForValue().increment(key, -delta);
    }

    //================================Map=================================

    /**
     * HashGet
     *
     * @param key  键 不能为null
     * @param item 项 不能为null
     * @return 值
     */
    public Object hget(String key, String item) {
        return redisTemplate.opsForHash().get(key, item);
    }

    /**
     * 获取hashKey对应的所有键值
     *
     * @param key 键
     * @return 对应的多个键值
     */
    public Map<Object, Object> hmget(String key) {
        return redisTemplate.opsForHash().entries(key);
    }

    /**
     * 获取hashKey对应的所有键的数量
     *
     * @param key 键
     * @return 对应的所有键的数量
     */
    public Long hlen(String key){
        return redisTemplate.opsForHash().size(key);
    }

    /**
     * HashSet
     *
     * @param key 键
     * @param map 对应多个键值
     * @return true 成功 false 失败
     */
    public boolean hmset(String key, Map<Object, Object> map) {
        try {
            redisTemplate.opsForHash().putAll(key, map);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * HashSet 并设置时间
     *
     * @param key  键
     * @param map  对应多个键值
     * @param time 时间(秒)
     * @return true成功 false失败
     */
    public boolean hmset(String key, Map<Object, Object> map, long time, TimeUnit timeUnit) {
        try {
            redisTemplate.opsForHash().putAll(key, map);
            if (time > 0) {
                expire(key, time, timeUnit);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 向一张hash表中放入数据,如果不存在将创建
     *
     * @param key   键
     * @param item  项
     * @param value 值
     * @return true 成功 false失败
     */
    public boolean hset(String key, String item, Object value) {
        try {
            redisTemplate.opsForHash().put(key, item, value);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 向一张hash表中放入数据,如果不存在将创建
     *
     * @param key   键
     * @param item  项
     * @param value 值
     * @param time  时间(秒)  注意:如果已存在的hash表有时间,这里将会替换原有的时间
     * @return true 成功 false失败
     */
    public boolean hset(String key, String item, Object value, long time, TimeUnit timeUnit) {
        try {
            redisTemplate.opsForHash().put(key, item, value);
            if (time > 0) {
                expire(key, time, timeUnit);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 删除hash表中的值
     *
     * @param key  键 不能为null
     * @param item 项 可以使多个 不能为null
     */
    public void hdel(String key, Object... item) {
        redisTemplate.opsForHash().delete(key, item);
    }

    /**
     * 判断hash表中是否有该项的值
     *
     * @param key  键 不能为null
     * @param item 项 不能为null
     * @return true 存在 false不存在
     */
    public boolean hHasKey(String key, String item) {
        return redisTemplate.opsForHash().hasKey(key, item);
    }


    /**
     * hash递增 如果不存在,就会创建一个 并把新增后的值返回
     *
     * @param key  键
     * @param item 项
     * @return
     */
    public Long hincr(String key, String item, Long delta) {
        return redisTemplate.opsForHash().increment(key, item, delta);
    }


    /**
     * hash递减
     *
     * @param key  键
     * @param item 项
     * @param by   要减少记(小于0)
     * @return
     */
    public double hdecr(String key, String item, double by) {
        return redisTemplate.opsForHash().increment(key, item, -by);
    }

    /**
     * hash递减
     *
     * @param key  键
     * @param item 项
     * @return
     */
    public double hdecr(String key, String item, Long delta) {
        return redisTemplate.opsForHash().increment(key, item, -delta);
    }


    //============================set=============================

    /**
     * 根据key获取Set中的所有值
     *
     * @param key 键
     * @return
     */
    public Set<Object> sGet(String key) {
        try {
            return redisTemplate.opsForSet().members(key);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 根据value从一个set中查询,是否存在
     *
     * @param key   键
     * @param value 值
     * @return true 存在 false不存在
     */
    public boolean sHasKey(String key, Object value) {
        try {
            return redisTemplate.opsForSet().isMember(key, value);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 将数据放入set缓存
     *
     * @param key    键
     * @param values 值 可以是多个
     * @return 成功个数
     */
    public long sSet(String key, Object... values) {
        try {
            return redisTemplate.opsForSet().add(key, values);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    /**
     * 将set数据放入缓存
     *
     * @param key    键
     * @param time   时间(秒)
     * @param values 值 可以是多个
     * @return 成功个数
     */

    public long sSetAndTime(String key, long time, TimeUnit timeUnit, Object... values) {
        try {
            Long count = redisTemplate.opsForSet().add(key, values);
            if (time > 0) {
                expire(key, time, timeUnit);
            }
            return count;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    /**
     * 获取set缓存的长度
     *
     * @param key 键
     * @return
     */
    public long sGetSetSize(String key) {
        try {
            return redisTemplate.opsForSet().size(key);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    /**
     * 移除值为value的
     *
     * @param key    键
     * @param values 值 可以是多个
     * @return 移除的个数
     */
    public long setRemove(String key, Object... values) {
        try {
            Long count = redisTemplate.opsForSet().remove(key, values);
            return count;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public Set<Object> setMembers(String key) {
        if (!StringUtils.isEmpty(key)) {
            return redisTemplate.opsForSet().members(key);
        }
        return null;
    }

    //===============================list=================================

    public Object lPop(String key) {
        try {
            return redisTemplate.opsForList().leftPop(key);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    /**
     * 获取list缓存的内容
     *
     * @param key   键
     * @param start 开始
     * @param end   结束  0 到 -1代表所有值
     * @return
     */
    public List<Object> lGet(String key, long start, long end) {
        try {
            return redisTemplate.opsForList().range(key, start, end);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 获取list缓存的长度
     *
     * @param key 键
     * @return
     */
    public long lGetListSize(String key) {
        try {
            return redisTemplate.opsForList().size(key);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    /**
     * 通过索引 获取list中的值
     *
     * @param key   键
     * @param index 索引  index>=0时， 0 表头，1 第二个元素，依次类推；index<0时，-1，表尾，-2倒数第二个元素，依次类推
     * @return
     */
    public Object lGetIndex(String key, long index) {
        try {
            return redisTemplate.opsForList().index(key, index);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 将list放入缓存
     *
     * @param key   键
     * @param value 值
     * @return
     */
    public boolean lSet(String key, Object value) {
        try {
            redisTemplate.opsForList().rightPush(key, value);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 将list放入缓存
     *
     * @param key   键
     * @param value 值
     * @param time  时间(秒)
     * @return
     */
    public boolean lSet(String key, Object value, long time, TimeUnit timeUnit) {
        try {
            redisTemplate.opsForList().rightPush(key, value);
            if (time > 0) {
                expire(key, time, timeUnit);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 将list放入缓存
     *
     * @param key   键
     * @param value 值
     * @return
     */
    public boolean lSet(String key, List<Object> value) {
        try {
            redisTemplate.opsForList().rightPushAll(key, value);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 将list放入缓存
     *
     * @param key   键
     * @param value 值
     * @param time  时间(秒)
     * @return
     */
    public boolean lSet(String key, List<Object> value, long time, TimeUnit timeUnit) {
        try {
            redisTemplate.opsForList().rightPushAll(key, value);
            if (time > 0) {
                expire(key, time, timeUnit);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 根据索引修改list中的某条数据
     *
     * @param key   键
     * @param index 索引
     * @param value 值
     * @return
     */
    public boolean lUpdateIndex(String key, long index, Object value) {
        try {
            redisTemplate.opsForList().set(key, index, value);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 移除N个值为value
     *
     * @param key   键
     * @param count 移除多少个
     * @param value 值
     * @return 移除的个数
     */
    public long lRemove(String key, long count, Object value) {
        try {
            Long remove = redisTemplate.opsForList().remove(key, count, value);
            return remove;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    // ################ sorted set #################


    public boolean zSetAdd(String key, Object member, double scope) {
        if (StringUtils.isEmpty(key) || member == null) {
            return false;
        }
        return redisTemplate.opsForZSet().add(key, member, scope);
    }

    public Long zSetRemove(String key, Object member, double scope) {
        if (StringUtils.isEmpty(key) || member == null) {
            return -1L;
        }
        return redisTemplate.opsForZSet().remove(key, member);
    }

    public Long zRemoveRangeByScore(String key, double score) {
        if (StringUtils.isEmpty(key) || score < 0) {
            return -1L;
        }
        return redisTemplate.opsForZSet().removeRangeByScore(key, 0, score);
    }

    public boolean zSetHasMember(String key, Object member) {
        if (StringUtils.isEmpty(key) || member == null) {
            return false;
        }
        Long idx = redisTemplate.opsForZSet().rank(key, member);
        return idx != null && idx >= 0L;
    }

    // Get set of {@link Tuple}s in range from {@code start} to {@code end} from sorted set ordered from high to low.
    public Set<ZSetOperations.TypedTuple<Object>> zSetReverseRangeWithScores(String key) {
        if (StringUtils.isEmpty(key)) {
            return null;
        }
        return redisTemplate.opsForZSet().reverseRangeWithScores(key, 0L, -1L);
    }

    public Double zSetIncr(String key, Object member, double incr) {
        if (StringUtils.isEmpty(key) || member == null) {
            return 0.0;
        }
        if (!zSetHasMember(key, member)) {
            boolean success = zSetAdd(key, member, 0.0);
            if (incr < 0) {
                return 0.0;
            }
        }
        return redisTemplate.opsForZSet().incrementScore(key, member, incr);
    }

    /*==================== blue =============================*/
    /**
     * 获取所有对应匹配键的集合
     *
     * @param pattern 匹配规则
     * @return 返回集合
     */
    public Set<String> getKeys(final String pattern) {
        return redisTemplate.keys(pattern);
    }

    /**
     * 批量删除key
     *
     * @param pattern 类似正则的匹配
     */
    public void removePattern(final String pattern) {
        Set<String> keys = redisTemplate.keys(pattern);
        if (null != keys && keys.size() > 0) {
            redisTemplate.delete(keys);
        }
    }

    /**
     * 批量删除对应的value
     *
     * @param keys key的集合
     */
    public void remove(Set<String> keys) {
        for (String key : keys) {
            del(key);
        }
    }

    /**
     * 存放整个list
     * @param key
     * @param values
     */
    public void setList(String key, List<String> values){
        redisTemplate.opsForList().rightPush(key, values);
    }

    /**
     * 存放一个进入list
     * @param key
     * @param value
     */
    public void setList(String key, String value){
        redisTemplate.opsForList().rightPush(key, value);
    }

    /**
     * 获取list
     * @param key
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<String> getList(String key){
        return (List<String>)redisTemplate.opsForList().rightPop(key);
    }


    /*==================== BitMap =============================*/
    /**
     * @description 给一个指定key的值得第offset位 赋值为value。
     * @param key 键
     * @param offset 偏移量
     * @param value 值
     * @return boolean
     */
    public boolean setBit(String key,long offset,boolean value){
        try{
            return redisTemplate.opsForValue().setBit(key,offset,value);
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    /**
     * @description 返回一个指定key的二进制信息
     * @param key 键
     * @param offset 偏移量
     * @return boolean
     */
    public boolean getBit(String key,long offset){
        try{
            return redisTemplate.opsForValue().getBit(key,offset);
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    /**
     * @description 返回一个指定key中位的值为1的个数
     * @param key 键
     * @return java.lang.Long
     */
    public Long bitCount(String key){
        try{
            return redisTemplate.execute(new RedisCallback<Long>() {
                @Override
                public Long doInRedis(RedisConnection redisConnection){
                    return redisConnection.bitCount(key.getBytes());
                }
            });
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    /**
     * @description 把Redis字符串当作位数组，并能对变长位宽和任意未字节对齐的指定整型位域进行寻址
     * @param key 键
     * @param operate GET/SET
     * @param isSign 是否为有符号数
     * @param type
     * @param offset
     * @return java.util.List<java.lang.Long>
     */
    public List<Long> bitField(String key, String operate, boolean isSign, int type, Long offset){
        try{
            //[GET type offset] // 获取指定位的值
            //[SET type offset value] // 设置指定位的值
            try{
                return getResource().bitfield(key,"GET", "u30", "0");
               /* return redisTemplate.execute(new RedisCallback<List<Long>>() {
                    @Override
                    public List<Long> doInRedis(RedisConnection redisConnection){
                        BitFieldSubCommands.BitFieldSubCommand bitFieldSubCommand = new BitFieldSubCommands.BitFieldSubCommand() {
                            @Override
                            public String getCommand() {
                                return operate;
                            }

                            @Override
                            public BitFieldSubCommands.BitFieldType getType() {
                                if(isSign){
                                    return BitFieldSubCommands.BitFieldType.signed(type);
                                }else{
                                    return BitFieldSubCommands.BitFieldType.unsigned(type);
                                }
                            }

                            @Override
                            public BitFieldSubCommands.Offset getOffset() {
                                return BitFieldSubCommands.Offset.offset(offset);
                            }
                        };
                        return redisConnection.bitField(key.getBytes(),BitFieldSubCommands.create());
                    }
                });*/
            }catch (Exception e){
                e.printStackTrace();
                return null;
            }
            //return getResource().bitfield(key,type,operate,offset);
            //用GET指令对超出当前字符串长度的位（含key不存在的情况）进行寻址，执行操作的结果会对缺失部分的位（bits）赋值为0。
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    /**
     * @description 返回字符串里面第一个被设置为1或者0的bit位。
     * @param key 键
     * @param value 值
     * @return java.lang.Long
     */
    public Long bitpos(String key,boolean value){
        try{
            return getResource().bitpos(key,value);
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    /*==================== Hyperloglog =============================*/
    /**
     * @description 计数
     * @param key
     * @param value
     * @return void
     */
    public Long addHyperloglog(String key, Object... value) {
        try {
            return redisTemplate.opsForHyperLogLog().add(key,value);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    /**
     * @description 获取总数
     * @param key
     * @return java.lang.Long
     */
    public Long HyperloglogSize(String key) {
        try {
            return redisTemplate.opsForHyperLogLog().size(key);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    /**
     * @description 删除
     * @param key
     * @return java.lang.Long
     */
    public boolean delHyperloglog(String key) {
        try {
            redisTemplate.opsForHyperLogLog().delete(key);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

}
