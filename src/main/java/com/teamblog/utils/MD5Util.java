package com.teamblog.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @create 2021-06-20 19:55
 */
public class MD5Util {

    public static String code(String str){//MD5加密算法，用于将字符串加密
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(str.getBytes());
            byte[] byteDigest = md.digest();
            int i;
            StringBuffer buf = new StringBuffer("");
            for (int offset=0;offset<byteDigest.length;offset++){
                i = byteDigest[offset];
                if(i<0){
                    i+=256;
                }
                if(i<16){
                    buf.append("0");
                }
                buf.append(Integer.toHexString(i));
            }
            //32位加密
            return buf.toString();

            //16位加密
//            return buf.toString().substring(8,24);
        }catch (NoSuchAlgorithmException e){
            e.printStackTrace();
            return null;
        }
    }


    public static void main(String[] args) {
        System.out.println(code("8675312432"));
    }


}
