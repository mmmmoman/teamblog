package com.teamblog.controller;

import com.teamblog.common.enums.CodeEnum;
import com.teamblog.entity.dto.JSONResponse;
import com.teamblog.entity.model.User;
import com.teamblog.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * @author lixin
 * @since 2021/7/20 9:06
 */
@Controller
public class LoginController {

    @Autowired
    UserService userService;

    /**
     * 异步验证注册时用户名是否重复
     * @param map
     * @return
     */
    @ResponseBody
    @PostMapping("/checkUsernameIfExist")
    public JSONResponse checkUsernameIfExist(@RequestBody Map<String, String> map){
        String username = map.get("username");
        boolean isExisted = userService.checkUsernameIfExist(username);
        return isExisted ? new JSONResponse().fail(CodeEnum.USER_IS_EXISTED) : new JSONResponse().success();
    }

    /**
     * 前往注册页
     * @return
     */
    @GetMapping("/toRegister")
    public String toRegister(){
        return "register";
    }

    /**
     * 实现异步注册功能
     * @param user
     * @return
     */
    @ResponseBody
    @PostMapping("/register")
    public JSONResponse register(@RequestBody User user){
        boolean isSuccess = false;
        if (user != null) {
            isSuccess = userService.saveUser(user);
        }
        return isSuccess ? new JSONResponse().success() : new JSONResponse().fail(CodeEnum.FAIL, "注册失败");
    }

    /**
     * 前往登录页
     * @return
     */
    @GetMapping("/toLogin")
    public String toLogin(){
        return "login";
    }

    /**
     * 登录功能
     * 登录成功回到首页，并将用户信息存到session中和携带到首页
     * 登录失败携带错误信息回到登录页
     * @param request
     * @param username
     * @param password
     * @param model
     * @return
     */
    @PostMapping("/login")
    public String login(HttpServletRequest request,
                        @RequestParam("username") String username,
                        @RequestParam("password") String password,
                        Model model){
        boolean isValid = userService.checkUser(username, password);
        if (isValid) {
            User user = userService.getUserByUsernameAndPassword(username, password);
            // 将当前登录的用户存到session中
            request.getSession().setAttribute("currentUser", user);
            // 转发到首页
            return "forward:/index";
        }
        // 将错误信息传回登录页
        model.addAttribute("errorMsg", "用户名或密码错误");
        return "login";
    }

    /**
     * 注销用户信息
     * 并删除用户的session信息
     * 跳转到首页
     * @param request
     * @return
     */
    @GetMapping("/logout")
    public String logout(HttpServletRequest request){
        request.getSession().removeAttribute("currentUser");
        return "redirect:/";
    }

}
