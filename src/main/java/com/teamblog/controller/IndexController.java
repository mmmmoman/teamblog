package com.teamblog.controller;

import com.teamblog.entity.model.Blog;
import com.teamblog.entity.vo.BlogIndexShowVo;
import com.teamblog.service.BlogService;
import com.teamblog.service.TagService;
import com.teamblog.service.TypeService;
import com.teamblog.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by limi on 2017/10/13.
 */
@Controller
public class IndexController {

    @Autowired
    private BlogService blogService;

    @Autowired
    private TypeService typeService;

    @Autowired
    private TagService tagService;

    @Autowired
    private UserService userService;

    /**
     * 未登录状态的首页跳转
     * @param model
     * @return
     */
    @GetMapping("/")
    public String index(Model model) {
        List<Blog> blogList = blogService.findAllBlog();
        List<BlogIndexShowVo> blogIndexShowVos = new ArrayList<>();

        for (Blog blog1 : blogList) {
            BlogIndexShowVo blogIndexShowVo = new BlogIndexShowVo();

            blogIndexShowVo.setId(blog1.getId());
            blogIndexShowVo.setContent(blog1.getContent());
            blogIndexShowVo.setTitle(blog1.getTitle());
            blogIndexShowVo.setUpdateTime(blog1.getUpdateTime());
            blogIndexShowVo.setViews(blog1.getViews());
            blogIndexShowVo.setType(typeService.getType(blog1.getTypeId()));
            blogIndexShowVo.setUser(userService.getUserById(blog1.getUserId()));
            blogIndexShowVos.add(blogIndexShowVo);
        }

        model.addAttribute("blogs",blogIndexShowVos);
        model.addAttribute("types",typeService.listTypeTop(6));
        model.addAttribute("tags",tagService.listTagsTop(10));
//        model.addAttribute("recommendblogs",blogService.listBlogTop(6));

        return "index";
    }

    /**
     * 登录后的首页跳转
     * @return
     */
    @PostMapping("/index")
    public String index(HttpServletRequest request,
                        Model model){
        List<Blog> blogList = blogService.findAllBlog();
        List<BlogIndexShowVo> blogIndexShowVos = new ArrayList<>();

        for (Blog blog1 : blogList) {
            BlogIndexShowVo blogIndexShowVo = new BlogIndexShowVo();

            blogIndexShowVo.setId(blog1.getId());
            blogIndexShowVo.setContent(blog1.getContent());
            blogIndexShowVo.setTitle(blog1.getTitle());
            blogIndexShowVo.setUpdateTime(blog1.getUpdateTime());
            blogIndexShowVo.setViews(blog1.getViews());
            blogIndexShowVo.setType(typeService.getType(blog1.getTypeId()));
            blogIndexShowVo.setUser(userService.getUserById(blog1.getUserId()));
            blogIndexShowVos.add(blogIndexShowVo);
        }

        model.addAttribute("blogs",blogIndexShowVos);
        model.addAttribute("types",typeService.listTypeTop(6));
        model.addAttribute("tags",tagService.listTagsTop(10));
        return "index";
    }

    /**
     * 查看博客详情
     * @param id
     * @param model
     * @return
     */
    @GetMapping("/blog/{id}")
    public String blog(@PathVariable Integer id, Model model) {
        Blog blog1 = blogService.getAndConvert(id);
        // 增加缓存中博客访问量
        blogService.increaseCacheBlogViewsById(id);
        model.addAttribute("blog",blog1);
        return "blog";
    }

    /**
     * 搜索博客
     * @param model
     * @param query
     * @return
     */
    @PostMapping("/search")
    public String search(Model model, @RequestParam String query){
        model.addAttribute("blogs",blogService.listBlogQuery(query));
        model.addAttribute("query",query);
        return "search";
    }

}
