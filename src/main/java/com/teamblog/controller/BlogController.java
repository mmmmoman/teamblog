package com.teamblog.controller;

import com.teamblog.entity.model.Blog;
import com.teamblog.entity.model.Tag;
import com.teamblog.entity.model.User;
import com.teamblog.entity.param.BlogInput;
import com.teamblog.entity.param.BlogQuery;
import com.teamblog.entity.vo.BlogListVo;
import com.teamblog.service.BlogService;
import com.teamblog.service.BlogTagService;
import com.teamblog.service.TagService;
import com.teamblog.service.TypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

/**
 * @create 2021-06-20 20:08
 */
@Controller
@RequestMapping("/admin")
public class BlogController {

    @Autowired
    private BlogService blogService;

    @Autowired
    private TypeService typeService;

    @Autowired
    private TagService tagService;

    @Autowired
    private BlogTagService blogTagService;

    /**
     * 博客列表
     * @param model
     * @return
     */
    @GetMapping("/blogs")
    public String blogs(Model model,HttpSession session){
        model.addAttribute("types",typeService.listType());

        User user = (User) session.getAttribute("currentUser");
        List<Blog> blogsList = blogService.getBlogByUser(user.getId());
        List<BlogListVo> blogs = new ArrayList<>();
        for (Blog blog : blogsList) {
            BlogListVo blogListVo = new BlogListVo();
            blogListVo.setId(blog.getId());
            blogListVo.setTitle(blog.getTitle());
            blogListVo.setTypeName((typeService.getType(blog.getTypeId())).getName());
            blogs.add(blogListVo);
        }
        model.addAttribute("blogs",blogs);
        return "admin/blogs";
    }


    @PostMapping("/blogs/search")
    public String search(Model model, BlogQuery blog){
        if(blog.getTitle().equals("") && blog.getTypeId()==null){
            List<Blog> blogs = blogService.findAllBlog();
            List<BlogListVo> blogListVos = new ArrayList<>();
            for (Blog blog1 : blogs) {
                BlogListVo blogListVo = new BlogListVo();
                blogListVo.setId(blog1.getId());
                blogListVo.setTitle(blog1.getTitle());
                blogListVo.setTypeName((typeService.getType(blog1.getTypeId())).getName());
                blogListVos.add(blogListVo);
            }
            model.addAttribute("blogs",blogListVos);
        }else {
            List<Blog> blogs = blogService.listBlog(blog);
            List<BlogListVo> blogListVos = new ArrayList<>();
            for (Blog blog1 : blogs) {
                BlogListVo blogListVo = new BlogListVo();
                blogListVo.setId(blog1.getId());
                blogListVo.setTitle(blog1.getTitle());
                blogListVo.setTypeName((typeService.getType(blog1.getTypeId())).getName());
                blogListVos.add(blogListVo);
            }
            model.addAttribute("blogs",blogListVos);
        }

        return "admin/blogs :: blogList";
    }

    @GetMapping("/blogs/input")
    public String input(Model model){
        model.addAttribute("types",typeService.listType());
        model.addAttribute("tags",tagService.listTag());
        model.addAttribute("blog",new Blog());
        return "admin/blogs-input";
    }


    @GetMapping("/blogs/{id}/input")
    public String editInput(@PathVariable Integer id, Model model){
        model.addAttribute("types",typeService.listType());
        model.addAttribute("tags",tagService.listTag());

        Blog blog = blogService.getBlog(id);
//        blog.init();
        model.addAttribute("blog",blog);
        return "admin/blogs-input";
    }


    @PostMapping("/blogs")
    public String post(BlogInput bloginput, HttpSession session, RedirectAttributes attributes){
        System.out.println(bloginput);
        Blog blog = new Blog();

//        blog.setUserId(((User) session.getAttribute("user")).getId());
        blog.setUserId(((User)(session.getAttribute("currentUser"))).getId());
        blog.setTypeId(bloginput.getTypeId());
        blog.setTitle(bloginput.getTitle());
        blog.setContent(bloginput.getContent());

        List<Tag> tagsById = new ArrayList<>();
        for (Integer tagId : bloginput.getTagIds()) {
            tagsById.add(tagService.getTag(tagId));
        }
        blog.setTags(tagsById);
        Integer blogId;
        if(bloginput.getId()==null){
            //新建blog的情况
            blogId = blogService.saveBlog(blog);
            //将blog和tag的映射关系保存到blogTag表
            for (Tag tag : tagsById) {
                blogTagService.saveBlogTag(blogId,tag.getId());
            }

        }else{//编辑blog的情况
            blogId = bloginput.getId();
            blogService.updateBlog(blogId,blog);

            //在blogTag表删除关系再重新建立
            blogTagService.deleteByBlog(blogId);
            for (Tag tag : tagsById) {
                blogTagService.saveBlogTag(blogId,tag.getId());
            }
        }
        if(blog == null){
            attributes.addFlashAttribute("message","新增失败");
        }else {
            attributes.addFlashAttribute("message","新增成功");
        }
        return "redirect:/admin/blogs";
    }

    @GetMapping("/blogs/{id}/delete")
    public String delete(@PathVariable Integer id, RedirectAttributes attributes){
        blogService.deleteBlog(id);
        attributes.addFlashAttribute("message","删除成功");
        return "redirect:/admin/blogs";
    }



}
