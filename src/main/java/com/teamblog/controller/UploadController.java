package com.teamblog.controller;

import com.teamblog.common.enums.CodeEnum;
import com.teamblog.entity.dto.JSONResponse;
import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

/**
 * @author koitomi
 * @since 2021/7/20 10:55
 */
@Controller
public class UploadController {

    @RequestMapping("/toUpload")
    public String toUpload() {
        return "upload";
    }

    @ResponseBody
    @PostMapping("/upload")
    public JSONResponse upload(@RequestBody MultipartFile file) throws FileNotFoundException {
        JSONResponse response = new JSONResponse();
        if(file != null && !file.isEmpty()) {
            //先指定上传路径，获取对应物理路径
            //String targetDirectory = System.getProperty("user.dir")+"\\src\\main\\resources\\static\\upload";
            //String path = ResourceUtils.getURL("classpath:").getPath()+"static/upload";
            //String targetDirectory = path.replace('/', '\\').substring(1,path.length());
            String targetDirectory = "C:/upload/";
            String temFileName = file.getOriginalFilename();

            int dot = temFileName.lastIndexOf('.');
            String ext = "";  //文件后缀名
            if ((dot > -1) && (dot < (temFileName.length() - 1))) {
                ext = temFileName.substring(dot + 1);
            }
            // 其他文件格式不处理
            if ("png".equalsIgnoreCase(ext) || "jpg".equalsIgnoreCase(ext) || "gif".equalsIgnoreCase(ext)) {
                // 重命名上传的文件名
                String targetFileName = renameFileName(temFileName);
                // 保存的新文件
                File target = new  File(targetDirectory + "/images/" + File.separator + targetFileName);
                System.out.println(target.getAbsolutePath());
                try {
                    // 保存文件
                    FileUtils.copyInputStreamToFile(file.getInputStream(), target);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                response.setStatus(true);
                response.setCode(CodeEnum.SUCCESS.getCode());
                response.setMessage("上传成功");
                response.setData("/upload/images/" + targetFileName);
                return response;
            }
            response.setStatus(false);
            response.setCode(CodeEnum.FAIL.getCode());
            response.setMessage("选择的文件不是图片，请重新选择！");
            return response;
        }
        response.setStatus(false);
        response.setCode(CodeEnum.FAIL.getCode());
        response.setMessage("未选择图片！");
        return response;
    }

    /*文件重命名方法*/
    public static String renameFileName(String fileName) {
        String formatDate = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()); // 当前时间字符串
        int random = new Random().nextInt(10000);
        String extension = fileName.substring(fileName.lastIndexOf(".")); // 文件后缀
        return formatDate + random + extension;
    }

}
