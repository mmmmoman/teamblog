package com.teamblog.controller;

import com.teamblog.common.enums.CodeEnum;
import com.teamblog.entity.dto.JSONResponse;
import com.teamblog.entity.param.LikeBlog;
import com.teamblog.service.UserLikeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author lixin
 * @since 2021/7/21 16:03
 */
@Controller
public class LikeController {

    @Autowired
    UserLikeService userLikeService;

    @ResponseBody
    @PostMapping("/like")
    public JSONResponse like(@RequestBody LikeBlog likeBlog){
        JSONResponse response = new JSONResponse();
        if (likeBlog != null) {
            userLikeService.like(likeBlog.getUserId(), likeBlog.getBlogId());
            Long likeCount = userLikeService.countCacheLikeByBlogId(likeBlog.getBlogId());
            return response.success().setData(likeCount);
        }
        return response.fail(CodeEnum.FAIL, "参数为null");
    }

}
