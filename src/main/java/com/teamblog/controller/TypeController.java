package com.teamblog.controller;

import com.teamblog.entity.model.Type;
import com.teamblog.service.TypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * @create 2021-06-21 13:27
 */
@Controller
@RequestMapping("/admin")
public class TypeController {

    @Autowired
    private TypeService typeService;


    @GetMapping("/types")
    public String list(Model model){
        model.addAttribute("page",typeService.listType());
        return "admin/types";
    }

    @GetMapping("/types/input")
    public String input(Model model){
        model.addAttribute("type",new Type());
        return "admin/types-input";
    }

    @PostMapping("/types")
    public String post(Type type, RedirectAttributes attributes){
        Type type1 = typeService.findTypeByName(type.getName());
        if(type1 != null){
            attributes.addFlashAttribute("error","失败：无法添加重复分类");
            return "redirect:/admin/types";
        }
        if(type == null){
            attributes.addFlashAttribute("message","添加失败");
        }else{
            typeService.saveType(type);
            attributes.addFlashAttribute("message","添加成功");
        }
        return "redirect:/admin/types";
    }

    @GetMapping("/types/{id}/input")
    public String editInput(Model model, @PathVariable Integer id){
        model.addAttribute("type",typeService.getType(id));
        return "admin/types-input";
    }

    @PostMapping("/types/{id}")
    public String editpost(Type type, RedirectAttributes attributes, @PathVariable Integer id){
        Type type1 = typeService.findTypeByName(type.getName());
        if(type1 != null){
            attributes.addFlashAttribute("error","失败：无法添加重复分类");
            return "redirect:/admin/types";
        }
        if(type == null){
            attributes.addFlashAttribute("message","更新失败");
        }else{
            typeService.updateType(id,type);
            attributes.addFlashAttribute("message","更新成功");
        }
        return "redirect:/admin/types";
    }

    @GetMapping("/types/{id}/delete")
    public String delete(@PathVariable Integer id, RedirectAttributes attributes){
        typeService.deleteType(id);
        attributes.addFlashAttribute("message","删除成功");
        return "redirect:/admin/types";
    }



}
