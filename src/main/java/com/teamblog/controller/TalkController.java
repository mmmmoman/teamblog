package com.teamblog.controller;

import com.github.pagehelper.PageInfo;
import com.teamblog.common.enums.CodeEnum;
import com.teamblog.entity.dto.JSONResponse;
import com.teamblog.entity.param.TalkInput;
import com.teamblog.entity.vo.UserTalkVo;
import com.teamblog.service.TalkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

/**
 * @author koitomi
 * @since 2021/7/20 15:36
 */

@Controller
public class TalkController {

    @Autowired
    TalkService talkService;

    @GetMapping("/toTalk")
    public String toTalk(Model model,
                         @RequestParam(defaultValue = "1", value = "pageNum") Integer pageNum){
        PageInfo<UserTalkVo> pageInfo = talkService.listAll(pageNum, 10);
        model.addAttribute("pageInfo", pageInfo);
        return "talk";
    }

    @PostMapping("/addTalk")
    @ResponseBody
    public JSONResponse addTalk(@RequestBody TalkInput talkInput){
        JSONResponse response = new JSONResponse();
        if(talkInput == null)
            return response.fail(CodeEnum.FAIL);
        if (talkService.saveTalk(talkInput)) {
            response.success();
        }
        return response;
    }
    
    @PostMapping("/deleteTalk")
    @ResponseBody
    public JSONResponse deleteTalk(@RequestBody TalkInput talkInput){
        System.out.println("id是："+talkInput.getContent());
        JSONResponse response = new JSONResponse();
        if (talkService.deleteTalk(talkInput.getUserId())) {
            response.success();
        }
        return response;
    }

}
