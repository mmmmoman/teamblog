package com.teamblog.controller;

import com.teamblog.common.enums.CodeEnum;
import com.teamblog.entity.dto.JSONResponse;
import com.teamblog.entity.model.User;
import com.teamblog.service.TalkService;
import com.teamblog.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * @author MMMmoMan
 * @create 2021-07-19 23:34
 */
@Controller
public class UserController {

    @Autowired
    UserService userService;

    @Autowired
    TalkService talkService;

    /**
     * 前往修改密码页面
     * @return
     */
    @GetMapping("/toUpdatePwd")
    public String toUpdatePwd(){
        return "updatePwd";
    }

    /**
     * 实现异步修改密码操作
     * @param map
     * @return
     */
    @ResponseBody
    @PostMapping("/updatePwd")
    public JSONResponse updatePwd(@RequestBody Map<String, String> map){
        String username = map.get("username");
        String currentPwd = map.get("currentPwd");
        String newPwd = map.get("newPwd");
        boolean isValid = userService.checkUser(username, currentPwd);
        JSONResponse response = new JSONResponse();
        if (isValid) {
            User user = userService.getUserByUsernameAndPassword(username, currentPwd);
            user.setPassword(newPwd);
            boolean isSuccess = userService.updateUser(user);
            return isSuccess ? response.success() : response.fail(CodeEnum.FAIL, "修改密码失败");
        }
        return response.fail(CodeEnum.FAIL, "当前密码不正确");
    }

/*-----------------个人中心-------------------*/

    @RequestMapping("/toPersonCenter")
    public String toPersonCenter(HttpServletRequest request){
        //获取最新用户信息
        HttpSession session = request.getSession();
        User user = (User)session.getAttribute("currentUser");
        session.setAttribute("currentUser",userService.getUserById(user.getId()));
        return "admin/person-center";
    }

    @PostMapping("/updateUserMsg")
    @ResponseBody
    public JSONResponse updateUserMsg(@RequestBody User user){
        JSONResponse response = new JSONResponse();
        User preUser = userService.getUserById(user.getId());
        if(user.getNickname()!=null) preUser.setNickname(user.getNickname());
        if(user.getEmail()!=null) preUser.setEmail(user.getEmail());
        if(user.getAvatar()!=null) preUser.setAvatar(user.getAvatar());
        if(user.getSex()!=null) preUser.setSex(user.getSex());
        if(userService.updateUser(preUser))
            response.success();
        return response;
    }


    @GetMapping("/personTalk")
    public String personTalk(HttpServletRequest request,Model model){
        HttpSession session = request.getSession();
        User user = (User)session.getAttribute("currentUser");
        model.addAttribute("personalTalks", talkService.listPersonalTalk(user.getId()));
        return "admin/person-talk";
    }


}
