package com.teamblog.controller;

import com.teamblog.entity.model.Tag;
import com.teamblog.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * @create 2021-06-21 13:27
 */
@Controller
@RequestMapping("/admin")
public class TagController {

    @Autowired
    private TagService tagService;


    @GetMapping("/tags")
    public String list(Model model){
        model.addAttribute("page",tagService.listTag());
        return "admin/tags";
    }

    @GetMapping("/tags/input")
    public String input(Model model){
        model.addAttribute("tag",new Tag());
        return "admin/tags-input";
    }

    @PostMapping("/tags")
    public String post(Tag tag, RedirectAttributes attributes){
        Tag tag1 = tagService.findTagByName(tag.getName());
        if(tag1 != null){
            attributes.addFlashAttribute("error","失败：无法添加重复分类");
            return "redirect:/admin/tags";
        }
        if(tag == null){
            attributes.addFlashAttribute("message","添加失败");
        }else{
            tagService.saveTag(tag);
            attributes.addFlashAttribute("message","添加成功");
        }
        return "redirect:/admin/tags";
    }

    @GetMapping("/tags/{id}/input")
    public String editInput(Model model, @PathVariable Integer id){
        model.addAttribute("tag",tagService.getTag(id));
        return "admin/tags-input";
    }

    @PostMapping("/tags/{id}")
    public String editpost(Tag tag, RedirectAttributes attributes, @PathVariable Integer id){
        Tag tag1 = tagService.findTagByName(tag.getName());
        if(tag1 != null){
            attributes.addFlashAttribute("error","失败：无法添加重复分类");
            return "redirect:/admin/tags";
        }
        if(tag == null){
            attributes.addFlashAttribute("message","更新失败");
        }else{
            tagService.updateTag(id,tag);
            attributes.addFlashAttribute("message","更新成功");
        }
        return "redirect:/admin/tags";
    }

    @GetMapping("/tags/{id}/delete")
    public String delete(@PathVariable Integer id, RedirectAttributes attributes){
        tagService.deleteTag(id);
        attributes.addFlashAttribute("message","删除成功");
        return "redirect:/admin/tags";
    }



}
