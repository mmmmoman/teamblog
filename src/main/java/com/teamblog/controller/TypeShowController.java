package com.teamblog.controller;

import com.teamblog.entity.model.Blog;
import com.teamblog.entity.model.Type;
import com.teamblog.entity.param.TypeTop;
import com.teamblog.entity.vo.BlogIndexShowVo;
import com.teamblog.service.BlogService;
import com.teamblog.service.TypeService;
import com.teamblog.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.ArrayList;
import java.util.List;

/**
 * @create 2021-06-28 15:37
 */
@Controller
public class TypeShowController {

    @Autowired
    TypeService typeService;

    @Autowired
    BlogService blogService;

    @Autowired
    UserService userService;

    @GetMapping("/types/{id}")
    public String types(Model model, @PathVariable Integer id){
        List<TypeTop> types = typeService.listTypeTop(10000);
        if(id == -1){
            id = types.get(0).getId();
        }
        model.addAttribute("types",types);

        List<Blog> blogs = blogService.listBlogByType(id);
        List<BlogIndexShowVo> blogIndexShowVos = new ArrayList<>();

        for (Blog blog1 : blogs) {
            BlogIndexShowVo blogIndexShowVo = new BlogIndexShowVo();

            blogIndexShowVo.setId(blog1.getId());
            blogIndexShowVo.setContent(blog1.getContent());
            blogIndexShowVo.setTitle(blog1.getTitle());
            blogIndexShowVo.setUpdateTime(blog1.getUpdateTime());
            blogIndexShowVo.setViews(blog1.getViews());
            blogIndexShowVo.setType(typeService.getType(blog1.getTypeId()));
            blogIndexShowVo.setUser(userService.getUserById(blog1.getUserId()));
            blogIndexShowVos.add(blogIndexShowVo);
        }

        model.addAttribute("blogs",blogIndexShowVos);
        model.addAttribute("activeType",id);

        return "types";
    }

}
