# 博客社区系统

## 背景

`博客社区系统`旨在为用户提供一个沟通交流、知识分享的平台。

> 在这里，你可以浏览到各类人群的想法，与之进行一番思想的碰撞
>
> 你也可以将自己的想法发表在社区博客中
>
> 你还可以在这里记录自己的心情动态，以便以后回过头来看那些心情愉悦亦或是烦恼的日子

这个系统目前实现了以下功能：

> 账户管理：用户注册、登录、退出登录、修改密码
>
> 个人中心：资料修改、查看个人动态、个人博客管理
>
> 博客浏览：首页预览博客、查看博客详情、根据关键字/类型/标签查询博客

此项目是在2021年7月13日~7月23日的毕业实习实训中完成的团队项目。该团队由自由组队的三人组成：谢同学、高同学、程同学

## 软件架构

涉及技术：Maven、SpringBoot、MyBatis、thymeleaf、MySQL、JDBC、Redis、AJAX、Layui、Semantic UI、Editor.md

## 安装说明

1、将此项目下载到本地

2、确保JDK8及以上版本，MySQL5.7及以上版本，Maven3.6及以上版本，Redis 5.0及以上版本

3、配置系统JDK环境，安装MySQL、Maven、Redis

4、使用Idea导入该项目

## 使用说明

本项目目前未部署到服务器，仅限于本地运行。

在idea中运行该项目，浏览器输入http://localhost:8080/即可访问

## 如何贡献

非常欢迎你的加入！提一个 Issue或者提交一个 Pull Request。

### 参与贡献者

感谢以下参与项目的人：

[<img src="https://portrait.gitee.com/uploads/avatars/user/2501/7505577_lixinxie_1595657915.png!avatar200" alt="alt text" title="codechap" style="zoom:50%;" />](https://gitee.com/codechap)

[<img src="https://portrait.gitee.com/uploads/avatars/user/2502/7506075_mmmmoman_1626767928.png!avatar200" alt="alt text" title="高山" style="zoom:50%;" />](https://gitee.com/mmmmoman)

[<img src="https://portrait.gitee.com/uploads/avatars/user/2513/7541832_koitomi_1626353202.png!avatar200" alt="alt text" title="koitomi" style="zoom:50%;" />](https://gitee.com/koitomi)